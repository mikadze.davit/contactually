import React, { Component } from "react";
import MapWrapper from "./components/MapWrapper";
import Map from "./components/Map";
import "./components/styles.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <MapWrapper>
          <Map />
        </MapWrapper>
      </div>
    );
  }
}

export default App;
