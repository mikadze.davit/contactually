import React, { Component } from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";

class Header extends Component {
  _onDragEnd = data => {
    this.props.onDragEnd(data);
  };
  render() {
    const { locations, setInputRef } = this.props;
    return (
      <div className="places-info">
        <DragDropContext onDragEnd={this._onDragEnd}>
          <input className="place-input" type="text" ref={setInputRef} />
          <Droppable droppableId="adresses-1" type="ADDRESSES">
            {(provided, snapshot) => (
              <div
                className="selected-locations"
                ref={provided.innerRef}
                {...provided.droppableProps}
              >
                {locations}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </div>
    );
  }
}

export default Header;
