import React, { Component, Fragment } from "react";
import Route from "./Route";
import Header from "./Header";

class Map extends Component {
  constructor(props) {
    super(props);
    this._map = null;
    this._directionsDisplay = null;
    this._directionsService = null;
    this._autocomplete = null;
    this._inputRef = null;
    this._autocompleteListener = null;

    this.state = {
      locations: []
    };
  }

  _setInputRef = ref => {
    this._inputRef = ref;
    this._autocomplete = new window.google.maps.places.Autocomplete(
      this._inputRef
    );
    this._autocompleteListener = this._autocomplete.addListener(
      "place_changed",
      this._handlePlaceSelect
    );
  };

  componentDidMount = () => {
    this._map = new window.google.maps.Map(this._mapNode, {
      zoom: 8,
      center: { lat: 41.7150115, lng: 44.7652063 }
    });

    this._directionsService = new window.google.maps.DirectionsService();
    this._directionsDisplay = new window.google.maps.DirectionsRenderer({
      draggable: true
    });

    this._directionsDisplayListener = this._directionsDisplay.addListener(
      "directions_changed",
      this._onDirectionsChanged
    );

    this._autocomplete.bindTo("bounds", this._map);
    this._autocomplete.setFields(["geometry", "name"]);
  };

  _handlePlaceSelect = () => {
    const place = this._autocomplete.getPlace();
    if (!place) return;

    if (place.geometry && place.geometry.location) {
      const marker = new window.google.maps.Marker({
        position: place.geometry.location,
        map: this._map
      });

      this._map.setCenter(place.geometry.location);
      this._map.setZoom(8);

      this.setState({
        locations: [...this.state.locations, { marker, name: place.name }]
      });

      this._calcRoute();
    }

    // reset input
    this._autocomplete.set("place", null);
    this._inputRef.value = "";
  };

  _calcRoute = () => {
    const length = this.state.locations.length;

    // Show regular markers if less than 2 locations
    if (length < 2) {
      this._setMapOnMarkers(this._map);
      return this._directionsDisplay.setMap(null);
    }

    // If more than 2 locations show directions draggable markers
    // Instead of regular markers
    this._setMapOnMarkers(null);
    this._directionsDisplay.setMap(this._map);

    const start = this.state.locations[0].marker.getPosition();
    const end = this.state.locations[length - 1].marker.getPosition();

    // if we have more than 2 routes we add all locations between
    // the first and last ones to waypoints
    let waypoints = [];
    for (let i = 0; i < length; i++) {
      if (i > 0 && i < length - 1) {
        waypoints.push({
          location: this.state.locations[i].marker.getPosition()
        });
      }
    }
    waypoints = waypoints.length ? waypoints : null;

    const request = {
      origin: start,
      destination: end,
      waypoints,
      travelMode: "DRIVING"
    };

    this._directionsService.route(request, (result, status) => {
      if (status === "OK") {
        this._directionsDisplay.setDirections(result);
      }
    });
  };

  _onDirectionsChanged = () => {
    let newRoutes = this._directionsDisplay.getDirections().routes[0].legs;
    newRoutes = [
      ...newRoutes.map(route => ({
        name: route.start_address,
        marker: new window.google.maps.Marker({
          position: route.start_location
        })
      })),
      {
        name: newRoutes[newRoutes.length - 1].end_address,
        marker: new window.google.maps.Marker({
          position: newRoutes[newRoutes.length - 1].end_location
        })
      }
    ];
    this.setState({
      locations: newRoutes
    });
  };

  _setMapOnMarkers = map => {
    for (let i = 0; i < this.state.locations.length; i++) {
      this.state.locations[i].marker.setMap(map);
    }
  };

  _removeMarkerByIndex = i => {
    this.state.locations[i].marker.setMap(null);
    this.setState(
      {
        locations: [
          ...this.state.locations.slice(0, i),
          ...this.state.locations.slice(i + 1)
        ]
      },
      () => this._calcRoute()
    );
  };

  _ondragEnd = data => {
    const { destination, source } = data;

    if (!destination || destination.index === source.index) return;

    const locations = this.state.locations;
    const draggedItem = locations[source.index];

    let newLocations = [
      ...locations.slice(0, source.index),
      ...locations.slice(source.index + 1)
    ];
    newLocations.splice(destination.index, 0, draggedItem);

    this.setState({
      locations: newLocations
    });

    this._calcRoute();
  };

  componentWillUnmount = () => {
    this._autocompleteListener && this._autocompleteListener.remove();
    this._directionsDisplayListener && this._directionsDisplayListener.remove();
  };

  render() {
    const locations = this.state.locations.map((location, i) => (
      <Route
        key={i}
        location={location}
        i={i}
        handleClick={() => this._removeMarkerByIndex(i)}
      />
    ));
    return (
      <Fragment>
        <Header
          onDragEnd={this._ondragEnd}
          setInputRef={this._setInputRef}
          locations={locations}
        />
        <div className="map-wrapper">
          <div ref={ref => (this._mapNode = ref)} id="map" />
        </div>
      </Fragment>
    );
  }
}

export default Map;
