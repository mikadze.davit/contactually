import React, { Component, Fragment } from "react";
import Spinner from "./Spinner";

class MapWrapper extends Component {
  state = {
    didLoad: false
  };
  _onInit = () => {
    this.setState({ didLoad: true });
  };
  componentDidMount = () => {
    ((d, s, id, cb) => {
      var js,
        fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return cb();
      }
      js = d.createElement(s);
      js.id = id;
      js.src =
        "//maps.googleapis.com/maps/api/js?key=AIzaSyCEMu_wKlNGjEipVehLvcKfpCdjYH4efKM&libraries=places";
      fjs.parentNode.insertBefore(js, fjs);
      js.onload = cb;
    })(document, "script", "google-maps", this._onInit);
  };
  render() {
    if (!this.state.didLoad) {
      return <Spinner />;
    }
    return <Fragment>{this.props.children}</Fragment>;
  }
}

export default MapWrapper;
