import React, { Component } from "react";
import { Draggable } from "react-beautiful-dnd";

class Route extends Component {
  render() {
    const { location, i, handleClick } = this.props;
    return (
      <Draggable draggableId={`${location.name}-${i}`} index={i}>
        {(provided, snapshot) => (
          <div
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            className="route"
          >
            <div className="route-index">{String.fromCharCode(i + 65)}: </div>
            <div className="location-text">{`${location.name}`}</div>
            <div className="route-remove" onClick={handleClick}>
              X
            </div>
          </div>
        )}
      </Draggable>
    );
  }
}

export default Route;
