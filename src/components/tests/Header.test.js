import React, { Component } from "react";
import { shallow, mount } from "enzyme";
import Header from "../Header";

describe("<Header />", () => {
  let called = 0;
  const locations = [
    <div className="location" key={1}>
      first
    </div>,
    <div className="location" key={2}>
      second
    </div>
  ];
  const onClick = () => called++;

  it("renders list container", () => {
    const wrap = shallow(<Header />);
    expect(wrap.find(".places-info")).toHaveLength(1);
  });

  it("renders input field", () => {
    const wrap = shallow(<Header />);
    expect(wrap.find("input.place-input")).toHaveLength(1);
  });

  it("executes setInputRef", () => {
    const wrap = mount(<Header locations={locations} setInputRef={onClick} />);
    expect(called).toEqual(1);
  });

  it("rendered provided locations list", () => {
    const wrap = mount(<Header locations={locations} setInputRef={onClick} />);
    expect(wrap.find("input.place-input")).toHaveLength(1);
    expect(wrap.find(".location")).toHaveLength(2);
  });

  it("matches snapshot", () => {
    const wrap = shallow(<Header locations={locations} setInputRef={onClick} />);
    expect(wrap).toMatchSnapshot();
  });
});
