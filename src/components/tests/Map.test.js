import React, { Component } from "react";
import { shallow, mount } from "enzyme";
import Map from "../Map";
import Header from "../Header";

describe("<Map />", () => {
  beforeAll(() => {
    return new Promise(resolve => {
      const head = document.getElementsByTagName("head")[0];
      head.appendChild(document.createElement("script"));
      const done = () => resolve();
      ((d, s, id, cb) => {
        var js,
          fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
          return cb();
        }
        js = d.createElement(s);
        js.id = id;
        js.src =
          "//maps.googleapis.com/maps/api/js?key=AIzaSyCEMu_wKlNGjEipVehLvcKfpCdjYH4efKM&libraries=places";
        fjs.parentNode.insertBefore(js, fjs);
        js.onload = cb;
      })(document, "script", "google-maps", done);
    });
  });

  it("renders header", () => {
    const wrap = mount(<Map />);
    expect(wrap.find(".map-wrapper")).toHaveLength(1);
  });

  it("renders header", () => {
    const wrap = mount(<Map />);
    expect(wrap.containsMatchingElement(<Header />)).toBeTruthy();
  });

  it("matches snapshot", () => {
    const wrap = mount(<Map />);
    expect(wrap).toMatchSnapshot();
  });
});
