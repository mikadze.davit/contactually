import React, { Component } from "react";
import { shallow, mount } from "enzyme";
import MapWrapper from "../MapWrapper";
import Spinner from "../Spinner";

beforeAll(() => {
  const head = document.getElementsByTagName("head")[0];
  head.appendChild(document.createElement("script"));
});

describe("<MapWrapper />", () => {
  it("renders spinner when loading", () => {
    const wrap = shallow(<MapWrapper />);
    wrap.setState({ didLoad: false });
    expect(wrap.containsMatchingElement(<Spinner />)).toBeTruthy();
  });

  it("renders children when finished loading", () => {
    const wrap = shallow(
      <MapWrapper>
        <div>hello</div>
      </MapWrapper>
    );
    wrap.setState({ didLoad: true });
    expect(wrap.containsMatchingElement(<div>hello</div>)).toBeTruthy();
  });

  it("matches snapshot", () => {
    const wrap = shallow(<MapWrapper />);
    expect(wrap).toMatchSnapshot();
  });
});
