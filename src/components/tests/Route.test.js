import React, { Component } from "react";
import { shallow, mount } from "enzyme";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import Route from "../Route";

describe("<Route />", () => {
  let called = 0;
  const location = {
    name: "Tbilisi"
  };
  const onClick = () => called++;
  const wrap = mount(
    <DragDropContext onDragEnd={() => {}}>
      <Droppable droppableId="adresses-1" type="ADDRESSES">
        {(provided, snapshot) => (
          <div
            className="selected-locations"
            ref={provided.innerRef}
            {...provided.droppableProps}
          >
            <Route location={location} i={0} handleClick={onClick} />
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );

  it("renders location name", () => {
    expect(wrap.find(".location-text").text()).toEqual("Tbilisi");
  });

  it("renders index", () => {
    expect(wrap.find(".route-index").text()).toEqual("A: ");
  });

  it("renders close", () => {
    expect(wrap.find(".route-remove")).toHaveLength(1);
  });

  it("executes handleClick on close click", () => {
    wrap.find('.route-remove').simulate('click')
    expect(called).toEqual(1)
  });
  
  it("matches snapshot", () => {
    expect(wrap).toMatchSnapshot();
  });
  
});
